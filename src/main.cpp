#include <QApplication>

#include "MainWindow.h"
#include "hughesnet.h"

int main(int argc, char *argv[]) {
    if(!initialize_curl()) {
        printf("Unable to initialize cURL library.\n");
        return EXIT_FAILURE;
    }

    QApplication app(argc, argv);
    MainWindow w;
    w.show();

    return app.exec();
}
