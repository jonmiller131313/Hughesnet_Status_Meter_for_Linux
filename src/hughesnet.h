#ifndef HUGHESNET_H
#define HUGHESNET_H

#include <stdexcept>
#include <string>
#include <sstream>
#include <map>

#include <cstdlib>
#include <cstring>
#include <stdint.h>

#include <curl/curl.h>
#include <curl/easy.h>

using namespace std;

typedef struct {
    bool throttled;
    time_t today;
    time_t resetDate;
    unsigned int tokensAvailable;
    unsigned int anytimeBandwidth;
    unsigned int anytimeLeft;
    unsigned int bonusBandwidth;
    unsigned int bonusLeft;
} hughesnet_data;

#define URL "http://www.systemcontrolcenter.com/getdeviceinfo/info.bin"

const static unsigned long BUFFER_SIZE = 10000 + sizeof(unsigned long);

namespace HUGHESNET {
    const static string TIME_LEFT = "TimeLeftUntilRefill";  // In minutes
    const static string THROTTLE_STATE = "FapThrottleState";  // 1 = Not throttled, 2 = Throttled
    const static string USER_TOKEN_REMAINING = "UserTokenBucketRemaining";
    const static string ANYTIME_PLAN = "AnyTimePlanAllowance";
    const static string ANYTIME_REMAINING = "AnyTimeAllowanceRemaining";
    const static string BONUS_PLAN = "BonusBytesPlanAllowance";
    const static string BONUS_REMAINING = "BonusBytesAllowanceRemaining";
}

void clean_up_curl();
bool initialize_curl();

void getData(hughesnet_data &data);

#endif
