#include "MainWindow.h"

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), refreshTimer(this) {
    ui.setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);

    // Add the refresh button and connect it
    QDialogButtonBox *buttons = findChild<QDialogButtonBox *>("buttons");
    QPushButton *refreshButton = new QPushButton("Refresh", this);
    connect(refreshButton, SIGNAL(clicked()), this, SLOT(refresh()));

    QAction *actionAbout = findChild<QAction*>("actionAbout");
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(displayAbout()));

    QAction *actionLicense = findChild<QAction*>("actionLicense");
    connect(actionLicense, SIGNAL(triggered()), this, SLOT(displayLicense()));

    buttons->addButton(refreshButton, QDialogButtonBox::ButtonRole::ActionRole);

    findChild<QCheckBox*>("checkThrottled")->setAttribute(Qt::WA_TransparentForMouseEvents);

    connect(&refreshTimer, &QTimer::timeout, this, &MainWindow::refresh);

    refreshTimer.start(1000 * 60);
    refresh();
}

MainWindow::~MainWindow() {
    clean_up_curl();
}

void MainWindow::displayAbout() {

    QString version = "1.0.1";
    QString author = "Jonathan Miller";
    QString email = "jonmiller131313@gmail.com";

    QString infoText = "Version: " + version + "\n" +
                        "Author: " + author + " \n" +
                        "Email: " + email + "\n" +
                        "Qt version: " + qVersion() + " (built against " +
                                                         QT_VERSION_STR + ")";

    QMessageBox aboutWindow(QMessageBox::Information, "About",
                            infoText, QMessageBox::Ok, this);
    aboutWindow.exec();
}

void MainWindow::displayLicense() {
    const static char *licenseText = {
#include "LICENSE.include"
    };
    QMessageBox licenseWindow(QMessageBox::Information, "Licence",
                              "This project is licenced under GPLv3+",
                              QMessageBox::Ok, this);

    licenseWindow.setText(licenseText);

    // Make the window wider
    QSpacerItem *horizontalSpacer = new QSpacerItem(550, 0, QSizePolicy::Minimum,
                                                    QSizePolicy::Expanding);
    QGridLayout *layout = (QGridLayout *)licenseWindow.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1,
                                    layout->columnCount());

    licenseWindow.exec();

}

void MainWindow::refresh() {
    printf("Refresh button pressed\n");
    hughesnet_data data;
    try {
        getData(data);
    } catch(const std::exception &e) {
        fprintf(stderr, "Caught exception with getData(): %s\n", e.what());
        QMessageBox::critical(this, "Error getting HughesNet data",
                              e.what());
        QCoreApplication::instance()->exit(1);
        exit(1);
    }

    static QPlainTextEdit *timeTextBox = findChild<QPlainTextEdit*>("textTime");
    static QPlainTextEdit *dateTextBox = findChild<QPlainTextEdit*>("textDate");
    static QPlainTextEdit *resetTextBox = findChild<QPlainTextEdit*>("textResetDate");
    static QCheckBox *throttleCheckBox = findChild<QCheckBox*>("checkThrottled");
    static QSpinBox *tokensSpinBox = findChild<QSpinBox*>("spinTokens");
    static QProgressBar *anytimeProgress = findChild<QProgressBar*>("progressAnytime");
    static QProgressBar *bonusProgress = findChild<QProgressBar*>("progressBonus");

    static const unsigned int TIME_BUFF_SIZE = 50;
    static char buff[TIME_BUFF_SIZE] = {0};
    strftime(buff, TIME_BUFF_SIZE, "%I:%M %p", localtime(&data.today));
    timeTextBox->setPlainText(buff);
    strftime(buff, TIME_BUFF_SIZE, "%d %b %Y", localtime(&data.today));
    dateTextBox->setPlainText(buff);
    strftime(buff, TIME_BUFF_SIZE, "%I:%M %p, %d %b %Y",
             localtime(&data.resetDate));
    resetTextBox->setPlainText(buff);

    throttleCheckBox->setChecked(data.throttled);
    tokensSpinBox->setValue(data.tokensAvailable);

    anytimeProgress->setFormat(QString().asprintf("%.2f GB / %u GB (%%p%%)",
                                                 (float) data.anytimeLeft / 1000,
                                                 data.anytimeBandwidth / 1000));
    anytimeProgress->setValue(data.anytimeLeft * 100 / data.anytimeBandwidth);
    bonusProgress->setFormat(QString().asprintf("%.2f GB / %u GB (%%p%%)",
                                               (float) data.bonusLeft / 1000,
                                               data.bonusBandwidth / 1000));
    bonusProgress->setValue(data.bonusLeft * 100 / data.bonusBandwidth);
}
