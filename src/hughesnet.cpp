#include "hughesnet.h"

static CURL *curl;

static uint8_t headerBuffer[BUFFER_SIZE] = {0};
static unsigned long *headerLen = (unsigned long *)headerBuffer;
static uint8_t dataBuffer[BUFFER_SIZE] = {0};
static unsigned long *dataLen = (unsigned long *)dataBuffer;

static size_t callback(char *curlBuffer, size_t s, size_t nitems, void *userData);

void clean_up_curl() {
    printf("Cleaning up the curl library.\n");
    if(curl)
        curl_easy_cleanup(curl);

    curl_global_cleanup();
}

bool initialize_curl() {
    if(CURLcode status = curl_global_init(CURL_GLOBAL_DEFAULT); status != CURLE_OK) {
        fprintf(stderr, "ERROR: curl_global_init() = %u: %s\n", status,
                curl_easy_strerror(status));
        return false;
    }

    curl = curl_easy_init();
    if(!curl) {
        fprintf(stderr, "ERROR: curl_easy_init() = NULL\n");
        clean_up_curl();
        return false;
    }
    int st = curl_easy_setopt(curl, CURLOPT_URL, URL);
    st += curl_easy_setopt(curl, CURLOPT_HEADERDATA, headerBuffer + sizeof(unsigned long));
    st += curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, callback);
    st += curl_easy_setopt(curl, CURLOPT_WRITEDATA, dataBuffer + sizeof(unsigned long));
    st += curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

    if(st != CURLE_OK) {
        fprintf(stderr, "ERROR: curl_easy_setopt() = %u: %s\n", st,
                curl_easy_strerror((CURLcode) st));
        clean_up_curl();
        return false;
    }

    printf("Curl library initialized successfully.\n");
    return true;
}

static size_t callback(char *curlBuffer, size_t s, size_t nitems,
                              void *userData) {
    char *dataBuff = (char *)userData;
    unsigned long size = s * nitems;
    unsigned long *offset = (unsigned long *)((uint8_t *)userData - sizeof(offset));

    if(*offset + size >= BUFFER_SIZE) {
        fprintf(stderr, "ERROR: callback(): Buffer overflow (%lu)\n",
                *offset + size);
        return 0;
    } else {
        printf("size = %lu\n", size);
    }

    memcpy(dataBuff + *offset, curlBuffer, size);
    *offset += size;

    return size;
}

void getData(hughesnet_data &data) {
    printf("Getting data from %s\n", URL);

    CURLcode status = curl_easy_perform(curl);
    if(status != CURLE_OK) {
        throw std::runtime_error("curl_easy_perform() returned " +
                                 std::to_string(status) + ": " +
                                 curl_easy_strerror(status));
    }
    printf("Got data, header len = %lu, data len = %lu \n", *headerLen, *dataLen);
    printf("Header:\n");
    for(unsigned int i = 0; i < *headerLen; i++) {
        putchar((headerBuffer + sizeof(headerLen))[i]);
    }
    printf("- - - - - DATA - - - - -\n");
    for(unsigned int i = 0; i < *dataLen; i++) {
        putchar((dataBuffer + sizeof(dataLen))[i]);
    }
    printf("- - - - - - - - - - - - -\n");

    printf("Parsing data.\n");
    istringstream dataStream(string((char *)dataBuffer + sizeof(dataLen), *dataLen));
    string line;
    map<string, string> list;
    string::size_type offset;

    while(getline(dataStream, line)) {
        offset = line.find('=');
        if(offset != string::npos) {
            list[line.substr(0, offset)] = line.substr(offset + 1);
        }
    }

    using namespace HUGHESNET;
    data.throttled = (bool) (stoi(list[THROTTLE_STATE]) - 1);
    data.tokensAvailable = stoi(list[USER_TOKEN_REMAINING]);
    data.anytimeBandwidth = stoi(list[ANYTIME_PLAN]);
    data.anytimeLeft = stoi(list[ANYTIME_REMAINING]);
    data.bonusBandwidth = stoi(list[BONUS_PLAN]);
    data.bonusLeft = stoi(list[BONUS_REMAINING]);

    data.today = time(NULL);
    time_t todayRounded = data.today - data.today % 60;
    unsigned int resetOffset = stoi(list[TIME_LEFT]);
    data.resetDate = todayRounded + resetOffset * 60;
    printf("Completed getting data\n");
    *headerLen = 0;
    *dataLen = 0;
}
