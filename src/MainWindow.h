#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QMessageBox>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QCheckBox>
#include <QAbstractButton>
#include <QSpinBox>
#include <QPlainTextEdit>
#include <QProgressBar>
#include <QTimer>

#include <string>

#include <cstdio>
#include <ctime>

#include "ui_MainWindow.h"

#include "hughesnet.h"

using namespace std;

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        // Constructor
        MainWindow(QWidget *parent = nullptr);

        ~MainWindow();

    private:
        Ui::MainWindow ui;

        QTimer refreshTimer;

    private slots:
        void refresh();

        void displayAbout();

        void displayLicense();

};

#endif
