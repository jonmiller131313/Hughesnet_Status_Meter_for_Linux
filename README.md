# HughesNet Status Meter for Linux
Due to the fact that HughesNet does not support Linux for their 
[Status Meter](http://services2.hughesnet.com/status_meter/)
program, this project aims to help relieve that.

This [post](https://www.dslreports.com/forum/r24144648-Hughesnet-Status-Meter-and-Linux)
from the [dslreports](https://dslreports.com) forums show that you can get information
regarding your bandwidth usage along with other account information from a
simple `wget` command, and this project's goal is to simplify everything into
one screen with little technical info.

## Setup

### Dependencies
 - g++
 - cmake
 - make
 - [Qt 5.11+](https://www.qt.io/download-qt-installer)

On systems using `apt` you can run
```bash
sudo apt install g++ cmake make
```

### Compiling
```bash
git clone https://gitlab.com/jonmiller131313/Hughesnet_Status_Meter_for_Linux.git # Clone the repo
cd Hughesnet_Status_Meter_for_Linux # cd into the directory
mkdir build && cd build # Make build directory
cmake .. # Generate Makefiles
make -j # Compile the project, executable is located in build/bin/
```

- - - - -

This project is [licensed](LICENSE) under GPLv3+.
